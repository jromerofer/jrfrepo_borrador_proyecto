//APITechU JRF

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser =require('body-parser'); // libreria para utilizar body en la API

app.use(bodyParser.json());
app.listen(port); // Levantamos el servidor para escuchar en un puerto
console.log("API escuchando en el puerto: " + port + " BBBEEEEEEPPPPPPP");

const userController = require('./controller/userController');
const authController = require('./controller/AuthController');

/** Los app.xxx() tienen que estar ordenados de más genérico a más específico para que el registro de rutas se haga bien. **/

// GET USERS
app.get('/apitechu/v1/users', userController.getUsersV1); //userController.getUsersV1 es la variable que indica la funcion.
// POST USERS V1 + V2
app.post('/apitechu/v1/users', userController.createUsersV1);
app.post('/apitechu/v2/users', userController.createUsersV2);
// POST DELETE
app.delete("/apitechu/v1/users/:id", userController.deleteUsersV1);
//LOGIN V1 + V2
app.post("/apitechu/v1/users/login", authController.loginV1);
app.post("/apitechu/v2/users/login", authController.loginV2);
//LOGOUT V1 + V2
app.post("/apitechu/v1/users/logout", authController.logoutV1);
app.post("/apitechu/v2/users/logout/:id", authController.logoutV2);
//GET USERS V2
app.get('/apitechu/v2/users', userController.getUsersV2);
//GET USERS V2 BY ID
app.get('/apitechu/v2/users/:id', userController.getUsersByV2);


//**********************************************//
// GET HELLO registro de ruta de API
app.get('/apitechu/v1/hello',
  function(req, res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde APITechU !! (:-P)  "});
  }
);


//POST MONSTRUO
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
   console.log("POST /apitechu/v1/monstruo/:p1/p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);
