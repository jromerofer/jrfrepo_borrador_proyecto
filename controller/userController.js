
const io = require('../io');
const crypt = require ('../crypt');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
const mLabAPIKey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
const requestJson = require('request-json');

//GET V1
function getUsersV1 (req, res) {
  console.log("GET /apitechu/v1/users");
  console.log(req.query);

  var result = {};
  var users = require('../users_pwds.json');

    if (req.query.$count == "true") {
      console.log("Count needed");
      result.count = users.length;
    }

  result.users = req.query.$top ?
  users.slice(0, req.query.$top) : users;

  res.send(result);
}

//POST CREATE USER
function createUsersV1 (req, res) {

  console.log("POST /apitechu/v1/users");
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
  }

  var users = require('../MOCK_DATA.json');
  users.push(newUser);
  io.writeUserDataToFile(users);

  console.log("Usuario añadido con éxito");
  res.send({"msg": "Usuario añadido con éxito"});
}

//POST CREATE USER V2
function createUsersV2 (req, res) {
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "pasword" : cry.hash(req.body.password)
  };

  var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente Creado");
    httpClient.post("user?" + mLabAPIKey, newUser,
      function(err, resMLab, body) {
        console.log("Usuario creado con éxito")
        res.send({"msg" : "Error obteniendo usuarios"})
      }

    );
}

//DELETE USER
function deleteUsersV1(req, res) {

  console.log("DELETE /apitechu/v1/users");
  var users = require('../MOCK_DATA.json');
  var index = 0;
    for (var value of users) {
      console.log(value.id);
      if (req.params.id == value.id) {
        console.log("ID de usuario a borrar encontrada " + value.id + "-->" + req.params.id);
        users.splice(index,1);
        console.log("Usuario borrado en el Array");
        io.writeUserDataToFile(users);

        console.log("Usuario borrado con éxito en fichero");
        res.send({"msg": "Usuario borrado con éxito en fihero"});

        break;
       } else {
          index ++;
          }
    }
}

//GET USER V2
function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente Creado");
  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response =!err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

//GET USERS V2 BY ID (Usamos un usuario ID para preguntar por el)
function getUsersByV2(req, res) {
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id" :' +  id + '}'; //comilla simple es un string literal. Estamos componiendo la query que le vamos a pasar

    var httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente Creado");
      httpClient.get("user?" + query + "&" + mLabAPIKey, // concatenamos la consulta
        function(err, resMLab, body) {

          if (err) {
            var response = {
              "msg" : "Error oteniendo usuario"
            }
            res.status(500);
          } else {
            if (body.lenght > 0) {
              var response =body[0];
              } else {
                var response = {
                  "msg" : "Usuario no encontrado"
                }
                res.status(404);
            }
          }
          res.send(response);
      }
    );
}

//GET USERS BY ID V2 - ACCOUNTS (Lo que devuelve son las cuentas del usuario, accounts)
function getAccountsV2(req, res) {
    console.log("GET /apitechu/v3/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userid" :' +  id + '}'; //comilla simple es un string literal. Estamos componiendo la query que le vamos a pasar

    var httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente Creado");
      httpClient.get("accounts?" + query + "&" + mLabAPIKey, // concatenamos la consulta
        function(err, resMLab, body) {

          if (err) {
            var response = {
              "msg" : "Error oteniendo cuentas"
            }
            res.status(500);
          } else {
            if (body.lenght > 0) {
              var response =body[0];
              } else {
                var response = {
                  "msg" : "El Usuario no tiene cuentas"
                }
                res.status(404);
            }
          }
          res.send(response);
      }
    );
}


//EXPORTS DE LAS LLAMADAS

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.createUsersV1 = createUsersV1;
module.exports.deleteUsersV1 = deleteUsersV1;

module.exports.getUsersByV2 = getUsersByV2;
module.exports.getAccountsV2 = getAccountsV2;

module.exports.createUsersV2 = createUsersV2;
